﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character : MonoBehaviour
{
    MoneyAmount ma;
    [SerializeField]
    private float speed;
    private int stamMlaku = 5;
    public int maxStam = 100;
    public int currStam;
    public StamBar stamina;
    public GameObject uang;
    public GameObject tidurUI;
    public Transform destinationStreet;
    public Transform destinationStreet2;
    public Transform destinationStreet2Back;
    public Transform destinationStreetBack;
    public Transform destinationKelas;
    public Transform destinationKantin;
    public Transform destinationHome;
    protected Animator anim;
    protected Vector2 direction;
    // Start is called before the first frame update
    void Start()
    {
        currStam = maxStam;
        stamina.SetMaxStamina(maxStam);
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        stamina.SetStamina(currStam);
        Move();
    }

    public void Move()
    {
        transform.Translate(direction * speed * Time.deltaTime);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Telehome" && currStam >= stamMlaku)
        {
            currStam -= stamMlaku;
            transform.position = destinationStreet.position;
        }

        if (collision.tag == "Telestreet" && currStam >= stamMlaku)
        {
            currStam -= stamMlaku;
            transform.position = destinationStreet2.position;
        }

        if (collision.tag == "Telestreet2" && currStam >= stamMlaku)
        {
            currStam -= stamMlaku;
            transform.position = destinationKelas.position;
        }

        if (collision.tag == "Telekantin" && currStam >= stamMlaku)
        {
            currStam -= stamMlaku;
            transform.position = destinationKantin.position;
        }

        if (collision.tag == "Telekelas" && currStam >= stamMlaku)
        {
            currStam -= stamMlaku;
            transform.position = destinationKelas.position;
        }

        if (collision.tag == "Telebstreet2" && currStam >= stamMlaku)
        {
            currStam -= stamMlaku;
            transform.position = destinationStreet2Back.position;
        }

        if (collision.tag == "Telebstreet" && currStam >= stamMlaku)
        {
            currStam -= stamMlaku;
            transform.position = destinationStreetBack.position;
        }

        if (collision.tag == "Telebhome" && currStam >= stamMlaku)
        {
            currStam -= stamMlaku;
            transform.position = destinationHome.position;
        }

        if (collision.tag == "Kasur" && currStam <= 20)
        {
            currStam += maxStam;
            tidurUI.SetActive(true);
            Time.timeScale = 0;
        }

        if(collision.tag == "uang")
        {
            uang.SetActive(false);
        }
    }
}
