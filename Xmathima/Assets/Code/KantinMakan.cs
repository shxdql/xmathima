﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KantinMakan : MonoBehaviour
{
    MoneyAmount duit;
    Character stamina;

    public int regoPecel = 8000;
    public int regoBakso = 7000;
    public int regoMiayam = 6000;
    public int regoNasgor = 9000;
    public GameObject kantinMakanUI;
    // Start is called before the first frame update
    void Start()
    {
        duit = GameObject.FindWithTag("GameController").GetComponent<MoneyAmount>();
        stamina = GameObject.FindWithTag("Player").GetComponent<Character>();
    }

    void OnTriggerEnter2D()
    {
        kantinMakanUI.SetActive(true);
        Cursor.visible = true;
    }

    void OnTriggerExit2D()
    {
        kantinMakanUI.SetActive(false);
        Cursor.visible = false;
    }

    public void TumbasPecel()
    {
        if (duit.money >= regoPecel)
        {
            duit.money -= regoPecel;
            stamina.currStam += 16;
        }
    }

    public void TumbasNasgor()
    {
        if (duit.money >= regoNasgor)
        {
            duit.money -= regoNasgor;
            stamina.currStam += 18;
        }
    }

    public void TumbasMiyayam()
    {
        if (duit.money >= regoMiayam)
        {
            duit.money -= regoMiayam;
            stamina.currStam += 12;
        }
    }

    public void TumbasBakso()
    {
        if (duit.money >= regoBakso)
        {
            duit.money -= regoBakso;
            stamina.currStam += 14;
        }
    }
}
