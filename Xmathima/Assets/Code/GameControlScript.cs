﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameControlScript : MonoBehaviour
{
    public GameObject disableObject;
    public bool disabled = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (disabled)
        {
            disableObject.SetActive(false);
        } else
        {
            disableObject.SetActive(true);
        }
    }
}
