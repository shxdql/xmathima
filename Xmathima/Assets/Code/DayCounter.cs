﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DayCounter : MonoBehaviour
{
    public int hari;

    GameObject dayUI;
    // Start is called before the first frame update
    void Start()
    {
        hari = 1;
        dayUI = GameObject.Find("JumlahHari");
    }

    // Update is called once per frame
    void Update()
    {
        dayUI.GetComponent<Text>().text = hari.ToString();
    }
}
