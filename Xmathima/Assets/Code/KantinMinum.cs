﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KantinMinum : MonoBehaviour
{
    MoneyAmount duit;
    Character stamina;

    public int regoEsteh = 2000;
    public int regoSoda = 3000;
    public int regoMilkshake = 4000;
    public int regoJus = 5000;
    public GameObject kantinMinumUI;
    // Start is called before the first frame update
    void Start()
    {
        duit = GameObject.FindWithTag("GameController").GetComponent<MoneyAmount>();
        stamina = GameObject.FindWithTag("Player").GetComponent<Character>();
    }

    void OnTriggerEnter2D()
    {
        kantinMinumUI.SetActive(true);
        Cursor.visible = true;
    }

    void OnTriggerExit2D()
    {
        kantinMinumUI.SetActive(false);
        Cursor.visible = false;
    }

    public void TumbasEsTeh()
    {
        if (duit.money >= regoEsteh)
        {
            duit.money -= regoEsteh;
            stamina.currStam += 4;
        }
    }

    public void TumbasSoda()
    {
        if (duit.money >= regoSoda)
        {
            duit.money -= regoSoda;
            stamina.currStam += 6;
        }
    }

    public void TumbasMilkShake()
    {
        if (duit.money >= regoMilkshake)
        {
            duit.money -= regoMilkshake;
            stamina.currStam += 8;
        }
    }

    public void TumbasJus()
    {
        if (duit.money >= regoJus)
        {
            duit.money -= regoJus;
            stamina.currStam += 10;
        }
    }
}
