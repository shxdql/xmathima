﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Character
{
    protected override void Update()
    {
        GetInput();
        base.Update();
    }
    private void GetInput()
    {
        direction = Vector2.zero;

        if (Input.GetKey(KeyCode.W))
        {
            anim.SetBool("isUp", true);
            anim.SetBool("isDown", false);
            anim.SetBool("isRight", false);
            anim.SetBool("isLeft", false);
            direction += Vector2.up;
        }
        if (Input.GetKey(KeyCode.S))
        {
            anim.SetBool("isUp", false);
            anim.SetBool("isDown", true);
            anim.SetBool("isRight", false);
            anim.SetBool("isLeft", false);
            direction += Vector2.down;
        }
        if (Input.GetKey(KeyCode.D))
        {
            anim.SetBool("isUp", false);
            anim.SetBool("isDown", false);
            anim.SetBool("isRight", true);
            anim.SetBool("isLeft", false);
            direction += Vector2.right;
        }
        if (Input.GetKey(KeyCode.A))
        {
            anim.SetBool("isUp", false);
            anim.SetBool("isDown", false);
            anim.SetBool("isRight", false);
            anim.SetBool("isLeft", true);
            direction += Vector2.left;
        }
    }
}