﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickMoney : MonoBehaviour
{
    MoneyAmount script;

    public int addMoney;

    // Start is called before the first frame update
    void Start()
    {
        script = GameObject.FindWithTag("GameController").GetComponent<MoneyAmount>();
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            script.money += addMoney;
            gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
