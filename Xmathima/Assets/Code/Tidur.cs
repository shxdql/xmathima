﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Tidur : MonoBehaviour
{
    DayCounter hari;
    public GameObject tidurUI;
    public GameObject disabledMoney;
    // Start is called before the first frame update
    void Start()
    {
        hari = GameObject.FindWithTag("GameController").GetComponent<DayCounter>();
    }

    public void Bangun()
    {
        hari.hari++;
        Time.timeScale = 1;
        tidurUI.SetActive(false);
        disabledMoney.SetActive(true);
    }

    public void BacktoMenu()
    {
        SceneManager.LoadScene(0);
    }
}
