﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PentolGoreng : MonoBehaviour
{
    MoneyAmount duit;
    Character stamina;

    public int rego = 2000;
    public GameObject pentolUI;
    // Start is called before the first frame update
    void Start()
    {
        duit = GameObject.FindWithTag("GameController").GetComponent<MoneyAmount>();
        stamina = GameObject.FindWithTag("Player").GetComponent<Character>();
    }

    void OnTriggerEnter2D()
    {
        pentolUI.SetActive(true);
        Cursor.visible = true;
    }

    void OnTriggerExit2D()
    {
        pentolUI.SetActive(false);
        Cursor.visible = false;
    }

    public void Tumbas()
    {
        if (duit.money >= rego)
        {
            duit.money -= rego;
            stamina.currStam += 4;
        }
    }
}
