﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour
{
    public GameObject pauseUI;

    public void PauseClick()
    {
        pauseUI.SetActive(true);
        Time.timeScale = 0f;
        this.gameObject.SetActive(false);
    }

    public void ResumeClick()
    {
        pauseUI.SetActive(false);
        Time.timeScale = 1f;
        this.gameObject.SetActive(true);
    }

    public void BacktoMenu()
    {
        SceneManager.LoadScene(0);
    }
}
