﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Batagor : MonoBehaviour
{
    // Start is called before the first frame update
    MoneyAmount duit;
    Character stamina;

    public int rego = 4000;
    public GameObject batagorUI;
    // Start is called before the first frame update
    void Start()
    {
        duit = GameObject.FindWithTag("GameController").GetComponent<MoneyAmount>();
        stamina = GameObject.FindWithTag("Player").GetComponent<Character>();
    }

    void OnTriggerEnter2D()
    {
        batagorUI.SetActive(true);
        Cursor.visible = true;
    }

    void OnTriggerExit2D()
    {
        batagorUI.SetActive(false);
        Cursor.visible = false;
    }

    public void Tumbas()
    {
        if (duit.money >= rego)
        {
            duit.money -= rego;
            stamina.currStam += 8;
        }
    }
}
