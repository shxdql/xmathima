﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoneyAmount : MonoBehaviour
{
    public int money;

    GameObject moneyUI;
    // Start is called before the first frame update
    void Start()
    {
        moneyUI = GameObject.Find("MoneyAmt");
    }

    // Update is called once per frame
    void Update()
    {
        moneyUI.GetComponent<Text>().text = money.ToString();
        if(money < 0)
        {
            money = 0;
        }
    }
}
