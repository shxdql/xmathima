﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cilok : MonoBehaviour
{
    MoneyAmount duit;
    Character stamina;

    public int rego = 3000;
    public GameObject cilokUI;
    // Start is called before the first frame update
    void Start()
    {
        duit = GameObject.FindWithTag("GameController").GetComponent<MoneyAmount>();
        stamina = GameObject.FindWithTag("Player").GetComponent<Character>();
    }

    void OnTriggerEnter2D()
    {
        cilokUI.SetActive(true);
        Cursor.visible = true;
    }

    void OnTriggerExit2D()
    {
        cilokUI.SetActive(false);
        Cursor.visible = false;
    }

    public void Tumbas()
    {
        if (duit.money >= rego)
        {
            duit.money -= rego;
            stamina.currStam += 6;
        }
    }
}
